<?php 
require_once './database.php';

function viewMouBaru(){

    $queryViewCatatanUser = "SELECT * FROM (SELECT F.ID_FAKTUR, M.NAMA_MITRA, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, F.PIC_PENS, F.TGL_MITRA, F.TGL_BERAKHIR
    FROM FAKTUR F
    FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
    FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
    FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
    WHERE F.ID_FAKTUR IS NOT NULL
    ORDER BY F.ID_FAKTUR DESC )  WHERE rownum <= 3";

    $resultQueryView = oci_parse(konekDb(), $queryViewCatatanUser);
    oci_execute($resultQueryView);

    $message = "";
    
    try {
        $count = 0;
        while ($viewDataCatatanUser = oci_fetch_object($resultQueryView)) {
            $message .= "Id  : " . $viewDataCatatanUser->ID_FAKTUR . PHP_EOL;
            $message .= "Nama mitra  : " . $viewDataCatatanUser->NAMA_MITRA . PHP_EOL;
            $message .= "Judul Kerjasama  : " . $viewDataCatatanUser->JUDUL_KERJASAMA . PHP_EOL;
            $message .= "Jenis Kerjasama  : " . $viewDataCatatanUser->NAMA_JENIS . PHP_EOL;
            $message .= "Alamat Mitra  : " . $viewDataCatatanUser->ALAMAT_MITRA . PHP_EOL;
            $message .= "PIC Mitra  : " . $viewDataCatatanUser->PIC_MITRA . PHP_EOL;
            $message .= "PIC PENS  : " . $viewDataCatatanUser->PIC_PENS . PHP_EOL;
            $message .= "\n";
            $count++;
        }

        if($count == 0) {
            $message = "Data tidak ditemukan";
        }
    
        return $message;
    } catch (\Throwable $th) {
        return $th;
    }
    /*
    if (oci_num_rows($resultQueryView)> 0) {
        while ($viewDataCatatanUser = oci_fetch_object($resultQueryView)) {

            $message .= "Id  : " . $viewDataCatatanUser->ID_FAKTUR . PHP_EOL;
            $message .= "Nama mitra  : " . $viewDataCatatanUser->NAMA_MITRA . PHP_EOL;
            $message .= "Judul Kerjasama  : " . $viewDataCatatanUser->JUDUL_KERJASAMA . PHP_EOL;
            $message .= "Jenis Kerjasama  : " . $viewDataCatatanUser->NAMA_JENIS . PHP_EOL;
            $message .= "Alamat Mitra  : " . $viewDataCatatanUser->ALAMAT_MITRA . PHP_EOL;
            $message .= "PIC Mitra  : " . $viewDataCatatanUser->PIC_MITRA . PHP_EOL;
            $message .= "PIC PENS  : " . $viewDataCatatanUser->PIC_PENS . PHP_EOL;
            $message .= "\n";
        }
    }
    else{
        $message = "Data tidak ditemukan";
    }

    return $message;
}*/
}
?>