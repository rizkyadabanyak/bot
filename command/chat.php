<?php  

namespace ChatTele;

require_once './database.php';

date_default_timezone_set('Asia/Jakarta');

function checkDataUser($id_user) {
    $querySelectData    = "SELECT * FROM TELE WHERE id_usertelegram = $id_user LIMIT 1";

    $resultQuery = oci_parse(konekDb(), $querySelectData);
    oci_execute($resultQuery);
    return (object) oci_fetch_object($resultQuery);
}

function checkEmail() {
    $querySelectDataEmail   = "SELECT EMAIL_USER FROM TELE";
    $hasilQuery = oci_parse(konekDb(), $querySelectDataEmail);
    oci_execute($hasilQuery);
    return (object) oci_fetch_object($hasilQuery);

    // return (object) oci_fetch_object($hasilQuery);
    // if(){
    //     return false;
    // }else{
    //     return true;
    // }
}

function checkDataUserEmail($email_user) {
    $querySelectDataEmail   = "SELECT EMAIL_USER FROM TELE WHERE EMAIL_USER = '$email_user'";
    $hasilQuery = oci_parse(konekDb(), $querySelectDataEmail);
    oci_execute($hasilQuery);
    return oci_fetch_object($hasilQuery);
}


function getIdUser() {
    $querySelectDataIdUser    = "SELECT DISTINCT id_usertelegram FROM TELE";
}

function updateDataUser($dataUser) {
    $queryUpdateDataUser   = "UPDATE TELE SET id_usertelegram = '$dataUser->id_user', nama = '$dataUser->nama' WHERE email_user = '$dataUser->email_user'";

    $hasilQueryDataUser = oci_parse(konekDb(), $queryUpdateDataUser);
    oci_execute($hasilQueryDataUser);
}

function getDataUser($user,$email_user) {
    $data   = [
        "id_user"   		=> 	$user->getId(),
        "nama"      		=> 	$user->getFirstName(),
        "email_user"      	=> 	$email_user
    ];
    return (object) $data;
}

?>