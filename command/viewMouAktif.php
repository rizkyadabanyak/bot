<?php 
require_once './database.php';

function viewMouAktif(){

    $sql="SELECT F.ID_FAKTUR, M.NAMA_MITRA, F.JUDUL_KERJASAMA, F.NAMA_DOKUMEN, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, 
    PP.NAMA_PIC_PENS, F.TGL_MITRA, F.TGL_BERAKHIR
    FROM FAKTUR F
    LEFT OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
    LEFT OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
    LEFT OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
    LEFT OUTER JOIN PIC_PENS PP ON  PP.ID_PIC_PENS = F.PIC_PENS
    WHERE F.ID_FAKTUR IS NOT NULL";

    $resultQueryView = oci_parse(konekDb(), $sql);
    oci_execute($resultQueryView);
    
    $message = "";
    try {
        $active = false;
        while ($rows = oci_fetch_object($resultQueryView)) {
           
            //  $message .= "Nama mitra  : " . $rows->NAMA_MITRA . PHP_EOL;
            //  $message .= "Tgl Berakhir  : " . $rows->TGL_BERAKHIR . PHP_EOL;
            //  $message .= "\n";
            //  $countInt++;

            $dateInt = strtotime($rows->TGL_BERAKHIR);
            $dateNow = strtotime(date('Y/m/d'));
            if ($dateInt - $dateNow < 0) {
                //data passive
                continue;
            }else {
            		$date1=date_create(date("Y-m-d"));
            		$date2=date_create($rows->TGL_BERAKHIR);
            		$c =date_diff($date2, $date1, true);

            		$result = (int)((int)$c->format("%a") / 30);
            		switch (true) {
            			case $result > 2:
            				//data aktif
                            $message .= "Nama mitra  : " . $rows->NAMA_MITRA . PHP_EOL;
                            $message .= "Tgl Berakhir  : " . $rows->TGL_BERAKHIR . PHP_EOL;
                            $message .= "Status : Active";
                            $message .= "\n";
                            $message .= "\n";
                           
                            $active = true;
            				break;
            			case $result <= 2 && $result > 0:
                            //data kurang 2 bulan
            				break;
            		}
            	}
        }


        if(!$active) {
            $message .= "Data tidak ditemukan";
        }
    
        return $message;
    } catch (\Throwable $th) {
        return $th;
    }
}


// function statusConditional(){
//     $test = $this->queryDetail();
//     $countDetail = count($test);
//     $status = [];
                    
//     foreach ($test as $value) {
//         for ($i = 0; $i < $countDetail; $i++) {
//             $dateInt = strtotime($test[$i]["TGL_BERAKHIR"]);
//             $dateNow = strtotime(date('Y/m/d'));
//              var_dump($dateInt - $dateNow);

//             if ($dateInt - $dateNow < 0) {
//                 $status[$i] = "Passive";
                
//                 continue;
//             } else {
            
//                 $date1=date_create(date("Y-m-d"));
//                 $date2=date_create($test[$i]["TGL_BERAKHIR"]);
//                 $c =date_diff($date2, $date1, true);
                                           
//                 $result = (int)((int)$c->format("%a") / 30);
//                 switch (true) {
//                     case $result > 2:
//                         $status[$i] = "Active";
//                         break;
//                     case $result <= 2 && $result > 0:
//                         $status[$i] = "2 month left";
//                         break;
//                 }
//             }
                                           
//         }
//     }
    
//     return $status;
// }

?>