<?php 
require_once './database.php';


function detailData($id_user,$nama_mitra){
	
	$queryCariDataCatatan = "SELECT F.ID_FAKTUR, M.NAMA_MITRA, M.EMAIL_MITRA, 
    M.HP_MITRA, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, F.PIC_PENS, 
    F.TGL_MITRA, F.TGL_BERAKHIR
    FROM FAKTUR F
    LEFT OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
    LEFT OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
    LEFT OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
    WHERE M.NAMA_MITRA = '$nama_mitra'";
    
    $resultQueryView = oci_parse(konekDb(), $queryCariDataCatatan);
    oci_execute($resultQueryView);

    $message = "";
    try {
        $count = 0;
        while ($viewDataCatatanUser = oci_fetch_object($resultQueryView)) {
            $message .= "ID : " . $viewDataCatatanUser->ID_FAKTUR . PHP_EOL;
            $message .= "Nama mitra  : " . $viewDataCatatanUser->NAMA_MITRA . PHP_EOL;
            $message .= "Email mitra  : " . $viewDataCatatanUser->EMAIL_MITRA . PHP_EOL;
            $message .= "No HP mitra  : " . $viewDataCatatanUser->HP_MITRA . PHP_EOL;
            $message .= "Judul Kerjasama  : " . $viewDataCatatanUser->JUDUL_KERJASAMA . PHP_EOL;
            $message .= "Jenis Kerjasama  : " . $viewDataCatatanUser->NAMA_JENIS . PHP_EOL;
            $message .= "Alamat Mitra  : " . $viewDataCatatanUser->ALAMAT_MITRA . PHP_EOL;
            $message .= "PIC Mitra  : " . $viewDataCatatanUser->PIC_MITRA . PHP_EOL;
            $message .= "PIC PENS  : " . $viewDataCatatanUser->PIC_PENS . PHP_EOL;
            // $message .= "Status  : " . $viewDataCatatanUser->NAMA_STATUS . PHP_EOL;
            $message .= "\n";
            $count++;
        }

        if($count == 0) {
            $message = "Data tidak ditemukan";
        }
      
        return $message;
    } catch (\Throwable $th) {
        return $th;
    }
}

?>