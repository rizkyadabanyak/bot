<?php 
require_once './database.php';

function viewMouPeringatan(){

    $date_now = date('Y/m/d');
    $date_sixMounthAgo = date('Y/m/d',strtotime("-180 day",strtotime(date('Y/m/d'))));
    // $queryViewCatatanUser="SELECT M.ID_MITRA,M.NAMA_MITRA, KM.ID_KEGIATAN_MITRA, M.HP_MITRA, M.EMAIL_MITRA, KM.TIMESTAMP
    // FROM MITRA M
    // LEFT OUTER JOIN KEGIATAN_MITRA KM ON M.ID_MITRA = KM.ID_MITRA
    // LEFT OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
    // WHERE KM.ID_KEGIATAN IS NULL
    // OR KM.ID_KEGIATAN_MITRA IN (SELECT MAX(ID_KEGIATAN_MITRA) FROM KEGIATAN_MITRA GROUP BY ID_MITRA)
    // AND KM.TIMESTAMP NOT BETWEEN '$date_sixMounthAgo' AND '$date_now'
    // ORDER BY M.ID_MITRA ASC";

    $queryViewCatatanUser="SELECT M.ID_MITRA,M.NAMA_MITRA, KM.ID_KEGIATAN_MITRA, M.HP_MITRA, M.EMAIL_MITRA, KM.TIMESTAMP
    FROM FAKTUR F
    LEFT OUTER JOIN MITRA M ON F.ID_MITRA= M.ID_MITRA
    LEFT OUTER JOIN KEGIATAN_MITRA KM ON KM.ID_FAKTUR= F.ID_FAKTUR
    LEFT OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
    WHERE KM.ID_KEGIATAN IS NULL
    OR KM.ID_KEGIATAN_MITRA IN (SELECT MAX(ID_KEGIATAN_MITRA) FROM KEGIATAN_MITRA GROUP BY ID_FAKTUR)
    AND KM.TIMESTAMP NOT BETWEEN '$date_sixMounthAgo' AND '$date_now'
    ORDER BY M.ID_MITRA ASC";

    $resultQueryView = oci_parse(konekDb(), $queryViewCatatanUser);
    oci_execute($resultQueryView);
    
   $message = "";
   try {
       $count = 0;
       while ($dataobject = oci_fetch_object($resultQueryView)) {

        $message .= "Nama mitra  : " .  $dataobject->NAMA_MITRA  . PHP_EOL;
        $message .= "Hp mitra  : " .  $dataobject->HP_MITRA . PHP_EOL;
        $message .= "Email mitra  : " .   $dataobject->EMAIL_MITRA . PHP_EOL;
    
        $message .= "\n";
        $count++;
    }

       if($count == 0) {
           $message = "Data tidak ditemukan";
       }
   
       return $message;
   } catch (\Throwable $th) {
       return $th;
   }
//  {
 }

?>