<?php 
require_once './database.php';

function viewMouAkanBerakhir(){

    $queryViewCatatanUser = "SELECT F.ID_FAKTUR, M.NAMA_MITRA, F.JUDUL_KERJASAMA, J.NAMA_JENIS, S.NAMA_STATUS
    FROM FAKTUR F
    FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
    FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
    FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
    FULL OUTER JOIN STATUS_MITRA S ON  S.ID_STATUS = F.ID_STATUS
    WHERE S.NAMA_STATUS = '2 month left'
    ";
     $resultQueryView = oci_parse(konekDb(), $queryViewCatatanUser);
     oci_execute($resultQueryView);

    $message = "";
    try {
        $count = 0;
        while ($viewDataCatatanUser = oci_fetch_object($resultQueryView)) {
            $message .= "Id  : " . $viewDataCatatanUser->ID_FAKTUR . PHP_EOL;
            $message .= "Nama mitra  : " . $viewDataCatatanUser->NAMA_MITRA . PHP_EOL;
            $message .= "Judul Kerjasama  : " . $viewDataCatatanUser->JUDUL_KERJASAMA . PHP_EOL;
            $message .= "Jenis Kerjasama  : " . $viewDataCatatanUser->NAMA_JENIS . PHP_EOL;
            $message .= "Status  : " . $viewDataCatatanUser->NAMA_STATUS . PHP_EOL;
            $message .= "\n";
            $count++;
        }

        if($count == 0) {
            $message = "Data tidak ditemukan";
        }
    
        return $message;
    } catch (\Throwable $th) {
        return $th;
    }
    
}

?>