<?php 
require_once './database.php';


function pushMessage(){
    viewCatatanUser();
}


function viewMouPeringatan(){

    $date_now = date('Y-m-d');
    $date_sixMounthAgo = date('Y-m-d',strtotime("-180 day",strtotime(date('Y-m-d'))));
    $queryViewCatatanUser = "SELECT M.nama_mitra 
    FROM kegiatan_mitra KM, mitra M, kegiatan K 
    WHERE KM.id_mitra = M.id_mitra AND KM.id_kegiatan = K.id_kegiatan 
    AND timestamp NOT BETWEEN $date_now AND $date_sixMounthAgo 
    GROUP BY M.nama_mitra";
    
    $resultQueryView      = mysqli_query(konekDb(), $queryViewCatatanUser);

    $message = "";

    if ($resultQueryView->num_rows > 0) {
        while ($viewDataCatatanUser = mysqli_fetch_assoc($resultQueryView)) {
            $resultCatatanUser = (object) $viewDataCatatanUser;
            
            $message .= "Nama mitra  : " . $resultCatatanUser->nama_mitra . PHP_EOL;
            $message .= "\n";

        }
    }
    else{
        $message = "Data tidak ditemukan";
    }

    return $message;
    
}

?>