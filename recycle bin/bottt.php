<?php

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;

require_once 'vendor/autoload.php';
require_once './database.php';

$configs = [
    "telegram" => [
        "token" => "5261645484:AAGveU8GLcYF2ErjRphJ4zvae2aDt73NKgY"
    ]
];

DriverManager::loadDriver(TelegramDriver::class);

$botman = BotManFactory::create($configs); 

// Command no @ to bot
$botman->hears("/start", function (BotMan $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();

    $bot->reply("Hai $firstname");
});

$botman->hears("/login {email_user}", function (Botman $bot, $email_user) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();
    $email_user = $email_user;

    include "command/chat.php";
    $dataDB	= ChatTele\checkDataUserEmail($email_user);
    if (!(object) $dataDB) {
        $bot->reply("Hai $firstname anda tidak bisa mengakses bot telegram");
    }else{

        $dataUser = ChatTele\getDataUser($user,$email_user);
        ChatTele\updateDataUser($dataUser);
        
        $bot->reply("Hai $firstname silahkan kirim pesan");
    }
});

include "command/viewIdUser.php";
$dataDBUser = viewIDUser();
$botman->group(['recipient' => $dataDBUser ], function($bot) {
        $bot->hears("/moubaru", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouBaru.php";
        
            $message = viewCatatanUser($id_user);
            $bot->reply($message);
        
        });
        $bot->hears("/mouaktif", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouAktif.php";
        
            $message = viewCatatanUser($id_user);
            $bot->reply($message);
        
        });
        
        $bot->hears("/mouperingatan", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouPeringatan.php";
        
            $message = viewCatatanUser($id_user);
            $bot->reply($message);
        
        });
        
        
        $bot->hears("/detaildata {nama_mitra}", function (Botman $bot, $nama_mitra) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            
            $nama_mitra = $nama_mitra;
            
            include "command/viewDetailData.php";
        
            $message = detailData($id_user, $nama_mitra);
            $bot->reply($message);
        });
        
        $bot->hears("/riwayatkegiatan {nama_mitra}", function (Botman $bot, $nama_mitra) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            $nama_mitra = $nama_mitra;
            
            include "command/viewRiwayatKegiatan.php";
        
            $message = RiwayatKegiatan($id_user, $nama_mitra);
            $bot->reply($message);
        });
    });



// command not found
$botman->fallback(function (BotMan $bot) {
    $message = $bot->getMessage()->getText();
    $bot->reply("Maaf, Perintah Ini '$message' Tidak Ada");
});


$botman->listen();