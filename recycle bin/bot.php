<?php

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;

require_once 'vendor/autoload.php';
require_once './database.php';

$configs = [
    "telegram" => [
        "token" => "5261645484:AAGveU8GLcYF2ErjRphJ4zvae2aDt73NKgY"
    ]
];

DriverManager::loadDriver(TelegramDriver::class);

$botman = BotManFactory::create($configs); 

// Command no @ to bot
$botman->hears("/start", function (BotMan $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();
    include "command/requestChat.php";
    
    $bot->reply("Hai $firstname (ID:$id_user),\n Ketikan perintah anda");
    include "command/requestChat.php";
});




$botman->hears("/help", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();
    $date_now = date('Ymd');
    
    
    $bot->reply("/lihat \n*Untuk Melihat Seluruh Catatan M.K");
});

$botman->hears("/moubaru", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    include "command/viewMouBaru.php";



});
$botman->hears("/mouaktif", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    include "command/viewMouAktif.php";


});

$botman->hears("/mouperingatan", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    include "command/viewMouPeringatan.php";



});


$botman->hears("/detaildata {nama_mitra}", function (Botman $bot, $nama_mitra) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    
    $nama_mitra = $nama_mitra;
    
    include "command/viewDetailData.php";

    $message = detailData($id_user, $nama_mitra);
    $bot->reply($message);
});

$botman->hears("/riwayatkegiatan {nama_mitra}", function (Botman $bot, $nama_mitra) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    $nama_mitra = $nama_mitra;
    
    include "command/viewRiwayatKegiatan.php";

    $message = RiwayatKegiatan($id_user, $nama_mitra);
    $bot->reply($message);
});


// ------------------------------------------------------------pembatas---------------------------------------------------------- 
// Command with @ to bot
$botman->hears("/start@K_coba_bot", function (BotMan $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();

    $id = $user->getId();

    $bot->reply("Hai $firstname (ID:$id_user),\n Ketikan perintah anda");
    include "command/requestChat.php";
});

$botman->hears("/help@K_coba_bot", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();


    $bot->reply("/lihat \n*Untuk Melihat Seluruh Catatan M.K");
});

$botman->hears("/moubaru@K_coba_bot", function (Botman $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $id_user = $user->getId();


    include "command/viewMouBaru.php";


});


// command not found
$botman->fallback(function (BotMan $bot) {
    $message = $bot->getMessage()->getText();
    $bot->reply("Maaf, Perintah Ini '$message' Tidak Ada");
});


$botman->listen();