<?php

use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;

require_once 'vendor/autoload.php';
require_once './database.php';

$configs = [
    "telegram" => [
        "token" => "5322994042:AAFwyfcBvJOZzPJ9Q6f1VvSIcj_ckVZ-t2I"
    ]
];

DriverManager::loadDriver(TelegramDriver::class);

$botman = BotManFactory::create($configs); 

$botman->hears("/start", function (BotMan $bot) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    
    $bot->reply("Hai $firstname" );
    $bot->reply("
    Silahkan login terlebih dahulu dengan menulis 

    /login {email yang sudah terdaftar}

    contoh : /login iduka@gmail.com

    
    " );
    
});



$botman->hears("/login {email_user}", function (Botman $bot, $email_user) {
    $user = $bot->getUser();
    $firstname = $user->getFirstName();
    $email_user = $email_user;

    // $bot->reply("$email_user");

    include "command/chat.php";
    $dataDB = ChatTele\checkDataUserEmail($email_user);
    // $bot->reply($dataDB->EMAIL_USER . " .... " . $email_user);

    // if(isset($dataDB->EMAIL_USER)){
    //     $bot->reply("Benar");
    // }else{
    //     $bot->reply("Salah");
    // }


    if(!isset($dataDB->EMAIL_USER)){
        $bot->reply("Hai $firstname anda tidak bisa mengakses bot telegram");
    }else{
        $dataUser = ChatTele\getDataUser($user,$email_user);
        ChatTele\updateDataUser($dataUser);
        
        $bot->reply("Anda berhasil login, Hai $firstname silahkan kirim pesan");
        $bot->reply("
        Perintah : 
        a. /moubaru = menampilan 3 data MoU terbaru
        b. /mouaktif = menampilkan data MoU yang berstatus aktif
        c. /mouakanberakhir = menampilkan data MoU yang akan berakhir dalam 2 bulan
        d. /mouperingatan = menampilan data MoU yang tidak melakukan kegiatan selama 6 bulan
        d. /detaildata {nama_mitra} =  menampilkan detail data mitra
        e. /riwayatkegiatan {nama_mitra} = menampilkan riwayat kegiatan mitra
    " );
    }
});

$botman->hears("/menu", function (BotMan $bot) {
    $bot->reply("
    Perintah : 
    a. /moubaru = menampilan 3 data MoU terbaru
    b. /mouaktif = menampilkan data MoU yang berstatus aktif
    c. /mouakanberakhir = menampilkan data MoU yang akan berakhir dalam 2 bulan
    d. /mouperingatan = menampilan data MoU yang tidak melakukan kegiatan selama 6 bulan
    d. /detaildata {nama_mitra} =  menampilkan detail data mitra
    e. /riwayatkegiatan {nama_mitra} = menampilkan riwayat kegiatan mitra
    ");
});

$botman->hears("/debug", function (Botman $bot) {
    $bot->reply('d');
});

include "command/viewIdUser.php";
$dataID = viewIDUser();
$botman->group(['recipient' => $dataID], function($bot) {
        $bot->hears("/moubaru", function (Botman $bot) {
            $user = $bot->getUser();
            $id_user = $user->getId();
        
            include "command/viewMouBaru.php";
        
            $message = viewMouBaru($id_user);
            $bot->reply($message);
        });

        $bot->hears("/mouaktif", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouAktif.php";
        
            $message = viewMouAktif($id_user);
            $bot->reply($message);
        
        });

        $bot->hears("/mouakanberakhir", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouAkanBerakhir.php";
        
            $message = viewMouAkanBerakhir($id_user);
            $bot->reply($message);
        
        });

        $bot->hears("/mouperingatan", function (Botman $bot) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            include "command/viewMouPeringatan.php";
        
            $message = viewMouPeringatan($id_user);
            $bot->reply($message);
        
        });

        $bot->hears("/detaildata {nama_mitra}", function (Botman $bot, $nama_mitra) {
            $user = $bot->getUser();
            $id_user = $user->getId();
        
            $nama_mitra = $nama_mitra;
            
            include "command/viewDetailData.php";
        
            $message = detailData($id_user, $nama_mitra);
            $bot->reply($message);
        });

        $bot->hears("/riwayatkegiatan {nama_mitra}", function (Botman $bot, $nama_mitra) {
            $user = $bot->getUser();
            $firstname = $user->getFirstName();
            $id_user = $user->getId();
        
            $nama_mitra = $nama_mitra;
            
            include "command/viewRiwayatKegiatan.php";
        
            $message = RiwayatKegiatan($id_user, $nama_mitra);
            $bot->reply($message);
        });
    });

$botman->fallback(function (BotMan $bot) {
    $message = $bot->getMessage()->getText();
    $bot->reply("Maaf, Perintah Ini '$message' Tidak Ada");
});


$botman->listen();